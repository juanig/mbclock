let now;

window.addEventListener('load', function() { updateClock(); });

setInterval(() => { updateClock(); }, 1000);

function updateClock() {
    const now = new Date();
    const options = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false };
    const time = now.toLocaleTimeString('es-UY', options);
    document.getElementById('time').innerHTML = time;
}